var path = require("path");
const webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var DIST_DIR = path.resolve(__dirname, "dist");
var SRC_DIR = path.resolve(__dirname, "src");

var config = {
	entry: {
	 main: SRC_DIR + "/index.js"
	},
	plugins: [
		  new webpack.DefinePlugin({ 
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }), 
    new webpack.optimize.AggressiveMergingPlugin(),
		 new webpack.optimize.ModuleConcatenationPlugin(),
		
			 new UglifyJsPlugin({
        test: /\.js($|\?)/i,
				 include: /\/includes/,
				 exclude: /\/excludes/,
				 cache: true,
				 parallel: true,
				 sourceMap: true
        }),
    new CompressionPlugin({
			cache: true,
			include: /\/includes/,
			exclude: /\/excludes/,
      asset: "[path].gz[query]",
			filename(assest){
				assest ='rename'
				return assest
			},
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
      threshold: 0,
      minRatio: 0.8,
			deleteOriginalAssets: true
    }),
      new HtmlWebpackPlugin({
			title: 'Production',
       title: 'Caching',
			 filename: 'index.html',
       minify: {
       removeComments: true,
       collapseWhitespace: true,
      },
    }),
			new webpack.optimize.CommonsChunkPlugin({
      name: 'entry',
      chunks: ['vendor', 'react', 'react-dom'],
      minChunks: Infinity,
    }),
    ],
    output: {
        path: DIST_DIR + "/app",
				chunkFilename: "[name].bundle.js",
        filename: "bundle.js",
        publicPath: "/dist/app/"
    },
    module: {
      loaders: [
				
        {
          exclude: [
            /\.sass$/
          ]
        },
        {
          test: /\.css$/,  
          include: /node_modules/,  
          loaders: ['style-loader', 'css-loader'],
        },
        {
          test: /\.scss$/,
          include: SRC_DIR,
          loader: ['style-loader', 'css-loader', 'sass-loader']
        },
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader?stage=0',
          query: {
            presets: [ 'react', 'es2015']
          }
        },
				{
					test: /\.(jpg|png|svg)$/,
          use: {
          loader: 'url-loader'
          }
				},
				{
				test: /\.(|ico|eot|svg|ttf|woff|woff2)$/,
					use: [
          {
            loader: 'url-loader?limit=100000',
          }
				],
				},
        {
          test: /\.(js|jsx)?$/,
          include: SRC_DIR,
          loader: "babel-loader",
          query: {
            presets: ['env', "react", "es2015", "stage-2"]
          }
        }
      ]
		}
};

module.exports = config;