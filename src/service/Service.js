import React, { Component } from 'react';
// import { Grid, Row, Col } from 'react-bootstrap';  

class Service extends Component {
  render() {
    return (
      <section id="services" style={{'minHeight': '678px'}}>
        <div className="container">
          <div className="text-center">
            <h2 className="section-heading">Yes! If you want to build ...</h2>
            <h3>
              an amazing web Application.
            </h3>
          </div>
          <div className="row">
            <div className="col-xs-12 col-md-4 col-sm-6 box1">
              <h4 className="text-center">
                Planning
              </h4>
              <p>
              At this stage we discuss about project idea, ask questions, determine goals. We discuss all the issues which may occur during development and give client feedback. We focus on scalibility of the product. Client provide the design or wireframes at this stage. Then we break the goals into small tasks and make a timeline, prepare a strategy and propose a optimal way to achive the goals. we use different project management tools for this like trello, ppivotal tracker, JIRA etc.
              </p>
            </div>
            <div className="col-xs-12 col-md-4 col-sm-6 box1">
              <h4 className="text-center">
                Design - Responsive
              </h4>
              <p>
                At this stage we design the web page based on the psd provided by the client. We make sure that the design is responsive and follow morden trends. If the client does not have design in mind, we put all our efforts to make it best design to match with the client's Brand concept. The client have full control on this creative phase.
              </p>
            </div>
            <div className="col-xs-12 col-md-4 col-sm-6 box1">
              <h4 className="text-center">
                Development - Agile
              </h4>
              <p>
                Based on project requirement and design provided by the client we start working on frontend and backend development and make it fully functional. This phase includes database architecture, backend development, frontend development, automated and manual testing, caching. We test it on testing tools to ensure all feature works perfecty. Also we test it on all devices and browsers available to make sure that it works as expected. We use Agile methods so that client can see his product even while development and can play with it and can request the changes if required.
              </p>
            </div>
            <div className="col-xs-12 col-md-4 col-sm-6 box1">
              <h4 className="text-center">
                Testing
              </h4>
              <p>
                Testing is an essential part of seamless functioning of website. We use automated testing tools as well as manual testing. We test each application using TDD or BDD. Afterwords we test the application on staging mode. After client's satisfaction we launch it in production mode and also test there to make sure that everythig works fine.
              </p>
            </div>
            <div className="col-xs-12 col-md-4 col-sm-6 box1">
              <h4 className="text-center">
                Launch
              </h4>
              <p>
                We launch the app in two phases. First we launch the application in staging mode and do all possible testing. After full satisfaction we launch the app for production. We use best tools to report bugs and follow them and fix as soon as possible.
              </p>
            </div>
            <div className="col-xs-12 col-md-4 col-sm-6 box1">
              <h4 className="text-center">
                Maintenance
              </h4>
              <p>
                We are always ready to resolve any unexpected situations occur. If any additional features or changes required to make the project better, we are always there.
              </p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Service;


