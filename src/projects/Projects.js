import React, { Component } from 'react';
import crowdstaffing from '../images/crowdstaffing.png';
import ratelock from '../images/ratelock.png';
import gametumor from '../images/gametumor.jpg';
import pv from '../images/pv.png';
import envia from '../images/envia.png';
import seofie from '../images/seofie.png';

class Projects extends Component {
  render() {
    return (
      <section id="projects" style={{'minHeight': '678px'}}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <h2 className="section-heading">Handcrafted by us</h2>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4">
              <h2 className="text-center">
                Crowdstaffing
              </h2>
              <img alt="Crowdstaffing" src={crowdstaffing}/>
              <div className="description">
                <p>
                  Crowdstaffing is a worldwide network of certified, skilled recruiters who takes out the best emerging talents from around the world.
                  It is embedded with rich choice of tech stacks like Ruby on Rails, Daxtra, Elasticsearch, Jenkins, Redis, Sidekiq, Twilio, PostgreSQL etc.
                </p>
                <div className="text-center">
                  <a className="btn btn-primary" href="https://jobs.crowdstaffing.com/#/" target="_blank">Visit Crowdstaffing</a>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <h2 className="text-center">
                Ratelock
              </h2>
              <img alt="Ratelock" src={ratelock}/>
              <div className="description">
                <p>
                  Ratelock introduces you with loan programs that fits to your pocket, provides quick funds within 48 hours of submitting your application, helps to purchase personal loans at minimum rates and it is bundled with tech stacks like ReactJS, Ruby on Rails, PosgreSQL, Sidekiq, Redis, Digital Ocean, NGNIX etc.
                </p>
                <div className="text-center">
                  <a className="btn btn-primary" href="https://www.ratelock.co.in" target="_blank">Visit Ratelock</a>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <h2 className="text-center">
                PrintVogue
              </h2>
              <img alt="PrintVogue" src={pv}/>
              <div className="description">
                <p>
                  Print Vogue is a collective of artists, printmakers and photographers dedicated to bringing digital art into the real world. It offers traditional giclée and c-type prints, as well as dye-sublimation and UV prints on the widest range of alternative materials like Bamboo, wood, paper, metal, glass, mirror and canvas.
                  <br/>
                  It is stacked with Ruby on Rails, Amazon, Ngnix, JQuery etc.
                </p>
                <div className="text-center">
                  <a className="btn btn-primary" href="http://PrintVogue.com" target="_blank">Visit PrintVogue.com</a>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4">
              <h2 className="text-center">
                Envia.do
              </h2>
              <img alt="Envia.do" src={envia}/>
              <div className="description">
                <p>
                  Envia.do is an online file-transferring platform. With Envia.do anyone can send up to 2GB per transfer. It’s simple, secure, free and good looking.
                  <br/>
                  It is stacked with Ruby on Rails, JQuery etc.
                </p>
                <div className="text-center">
                  <a className="btn btn-primary" href="http://envia.do" target="_blank">Visit Envia.do</a>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <h2 className="text-center">
                SEOfie
              </h2>
              <img alt="SEOfie" src={seofie}/>
              <div className="description">
                <p>
                SEOfie embedded with complex alogrithm analyze websites and generate detailed SEO reports for the website and helps online marketers &amp; business to achieve higher search engine rankings.
                <br/>
                  It is stacked with Ruby on Rails, MongoDB, Google Adsense, Google Analytics, Digital Ocean, Facebook, Twitter, LinkedIn, Trello etc.
                </p>
                <div className="text-center">
                  <a className="btn btn-primary" href="https://www.seofie.com/" target="_blank">Visit SEOfie.com</a>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <h2 className="text-center">
                Gametumor
              </h2>
              <img alt="Gametumor" src={gametumor}/>
              <div className="description">
                <p>
                  Gametumor.com is a reverse auction cum e-commerce service. It is an innovative, mathematical and skillful game placed on the online platform for the entertainment, rejuvenation and fun of the people at large by rewarding them for their skill set and mathematical abilities.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Projects;


