import React, { Component } from 'react';
import image from '../images/logo.png';

class Contact extends Component {

  componentDidMount(){
    // var mapCanvas = $('#map-canvas');
    // var myLatlng = new google.maps.LatLng(28.587303, 77.044662);

    // var mapOptions = {
    //   center: myLatlng,
    //   zoom: 14,
    //   mapTypeId: google.maps.MapTypeId.ROADMAP
    // }
    // var map = new google.maps.Map(mapCanvas[0], mapOptions);
    // var marker = new google.maps.Marker({
    //   position: myLatlng,
    //   map: map,
    //   draggable:false,
    //   title: 'Blazar Software Solutions',
    //   icon: image
    // });
  }

  render() {
    return (
      <section id="contact">
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <h1 className="cartoonx white-color ideax">
                If you have any Idea - Discuss with us
              </h1>
            </div>
          </div>
          <br/>
          <div className="row contact_us">
            <div className="col-md-6">
              <div id="map-canvas"></div>
            </div>
            <div className="col-md-6 text-center">
            <div className="tech cartoon white-color">
              Call us:
            </div>
            <br/>
            <a href="tel:+9101143022005"><i className="glyphicon glyphicon-earphone"></i> : +91 011 4302 2005</a>
            <br/>
            <br/>
            <a href="tel:+919910797825"><i className="glyphicon glyphicon-phone"></i> : +91 99 10 797825</a>
            <br/>
            <div className="tech cartoon white-color">
              Mail us:
            </div>
            <br/>
            <a href="mailto:suratpyari@blazarsol.com"><i className="glyphicon glyphicon-envelope"></i> : suratpyari@blazarsol.com</a>
            <br/>
            <div className="white-color">
              <div className="tech cartoon">
                Our home port:
              </div>
              <br/>
              <i className="glyphicon glyphicon-map-marker"/>
              <br/>
              208, Pocket 4,
              <br/>
              Dwarka Sector 11,
              <br/>
              New Delhi,
              <br/>
              Delhi,
              <br/>
              Postal Code: 110078
            </div>
            <br/>
            <span itemScope="" itemType="http://schema.org/Organization">
              <link href="http://www.blazarsol.com" itemProp="url"/>
              <a href="http://www.facebook.com/Blazar-Software-Solutions-513997562081748" itemProp="sameAs">
                <i className="fa fa-facebook"/>
              </a>
              <a href="https://www.linkedin.com/company/blazar-software-solutions" itemProp="sameAs">
                <i className="fa fa-linkedin"/>
              </a>
              <a href="https://plus.google.com/+Blazarsol" itemProp="sameAs">
                <i className="fa fa-google-plus"/>
              </a>
            </span>
          </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12 text-center">
            <h3 className="cartoon white-color join-usx">
              <a href="mailto:suratpyari@blazarsol.com">Join our amazing team</a>
            </h3>
          </div>
        </div>
      </section>
    );
  }
}

export default Contact;















