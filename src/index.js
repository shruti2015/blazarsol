import React from 'react';
import ReactDOM from 'react-dom';
import './styles/application.scss';
import App from './App.js';

ReactDOM.render(<App/>, document.getElementById('app'));
