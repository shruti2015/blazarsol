import React, { Component } from 'react';
// import { Grid, Row, Col } from 'react-bootstrap';

class Header extends Component {
  render() {
    return (
      <header id="page-top">
        <div className="container">
          <h1 className="intro-text">
            <div className="intro-lead-in">Get Explored with</div>
            <div className="intro-heading text-center">
              <div className="tech icon-logo"></div>
              <div className="text">
                <div className="tech icon-blazar"></div>
                <div className="tech icon-ss"></div>
              </div>
            </div>
            <a className="page-scroll btn btn-xl" href="#projects">Tell Me More</a>
          </h1>
        </div>
      </header>
    );
  }
}

export default Header;
