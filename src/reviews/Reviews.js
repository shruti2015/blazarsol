import React, { Component } from 'react';
// import MyMapComponent from './Testimonial';

class Reviews extends Component {

  componentDidMount(){
    // map = new google.maps.Map(document.getElementById('reviewmap'), {
    //   center: {lat: -34.397, lng: 150.644},
    //   zoom: 8
    // });
  }

  render() {
    return (
      <section id="reviews">
        <div className="container-fluid">
          <div className="text-center">
            <h2 className="section-heading">Reviews</h2>
          </div>
          <div id="reviewmap"/>
          <div className="row hidden comments">
            <div className="col-sm-12 col-md-6 comment">
              <div className="comment_content" style={{'maxHeight':'200px','overflowY':'auto'}}>
                <p>
                  "I have been working with Surat and her team at Blazar software solutions for several years now. She has been vital in bringing our ideas to life, and helping our business thrive. Surat and her team are courteous, professional, and extremely skilled at implementing the best software solutions for our small business, which is solely based online. They deliver great work, on time and on budget, which is why we hope to continue working with them as our company grows. I endorse their company with the highest recommendation.""
                </p>
              </div>
              <div className="text-right">
                <i>
                  Nathaniel Park, California
                  <br/>
                  Printvogue.com
                </i>
              </div>
              <hr/>
            </div>
            <div className="col-sm-12 col-md-6 comment">
              <div className="comment_content" style={{'maxHeight':'200px','overflowY':'auto'}}>
                <p>
                  "We have used Blazar Software services for web development for some of major clients in 2015. We were satisfied with the services offered and below we have a break down on their performance:
                </p>
                <p>
                  <b>
                    Quality and quantity of work:
                  </b>
                  a thorough analysis of the issues that needed to be solved, productive and goal attainment
                </p>
                <p>
                  <b>
                    Communication and interpersonal skills:
                  </b>
                  cooperation, and prompt respond to new projects and issues
                </p>
                <p>
                  <b>
                    Planning, administration and organization:
                  </b>
                  Completing goals in a rapid manner, prioritizing to complete most important task first
                </p>
                <p>
                  <b>
                    Field knowledge and expertise:
                  </b>
                  Blazar team has a vast knowledge base and researches new areas to develop further
                </p>
                <p>
                  <b>
                    Attitude:
                  </b>
                  dedication, loyalty, flexibility and initiative
                </p>
                <p>
                  <b>
                    Creative thinking:
                  </b>
                  innovative, receptive and originality
                </p>
                <p>
                  We recommend Blazar Software Services for companies who would like to create custom software solutions."
                </p>
              </div>
              <div className="text-right">
                <i>
                  M C, Austria
                </i>
              </div>
              <hr/>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Reviews;






