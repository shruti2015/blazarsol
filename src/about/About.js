import React, { Component } from 'react';
// import { Grid, Row, Col } from 'react-bootstrap';

class About extends Component {
  render() {
    return (
      <section id="about" style={{'minHeight': '678px'}}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <h2 className="section-heading">About</h2>
            </div>
          </div>
          <p className="text-center">
            <i>
              "A Blazar is a very compact quasar (radio source) associated with supermassive black hole at the
              <br/>
              center of an active, giant galaxy. Blazars are among the most energetic phenomena in the universe"
            </i>
          </p>
          <div className="row">
            <div className="col-lg-12 text-center">
              <h3>
                Blazar Software Solutions is a team full of
                <br/>
                Energy, Innovation, Passion, Excellence &amp; Commitment
                for the work.
                <br/>
                <br/>
              </h3>
            </div>
          </div>
          <h4 className="text-center"></h4>
          <p>
            We are Startup Software company focused on quality web development services. We have experience in web designing, web development, testing, architecture designing, requirement gathering and deployment. We have crested some awesome responsive sites for our esteemed clients. We have worked on variety of projects: e&shy;-commerce, social networking, educational, API integration, creating API for mobile devices etc.
          </p>
        </div>
      </section>
    );
  }
}

export default About;


