import React, { Component } from 'react';
// import { Grid, Row, Col } from 'react-bootstrap';

class Techologies extends Component {
  render() {
    return (
      <section className="bg-light-gray text-center" id="technologies" style={{'minHeight': '678px'}}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h2 className="section-heading">We Love Cutting Edge Web Technologies</h2>
            </div>
          </div>
          <h3>
            Backend Technologies
          </h3>
          <div className="logos text-center">
            <div className="tech tech-ruby">
              Ruby
            </div>
            <div className="tech tech-rails"></div>
            </div>
            <h3>
              Frontend Technologies
            </h3>
            <div className="logos text-center">
            <div className="tech tech-html-5"></div>
            <div className="tech tech-css3"></div>
            <div className="tech tech-jquery"></div>
            <div className="tech tech-coffee"></div>
            <div className="tech tech-angular"></div>
            <div className="tech tech-less"></div>
            <div className="tech tech-sass"></div>
            <div className="tech tech-bootstrap"></div>
          </div>
          <h3>
            Database
          </h3>
          <div className="logos text-center">
            <div className="tech tech-mysql"></div>
            <div className="tech tech-mongo-db"></div>
            <div className="tech tech-postgres"></div>
          </div>
          <h3>
            Search Engine
          </h3>
          <div className="logos text-center">
            <div className="tech tech-sphinx"></div>
            <div className="tech tech-elasticsearch"></div>
          </div>
          <h3>
            APIs
          </h3>
          <div className="logos text-center">
            <div className="tech tech-fb"></div>
            <div className="tech tech-in"></div>
            <div className="tech tech-twilio"></div>
            <div className="tech tech-twitter"></div>
            <div className="tech tech-youtube"></div>
            <div className="tech tech-payu"></div>
            <div className="tech tech-paypal"></div>
            <div className="tech tech-maps"></div>
            <div className="tech tech-braintree"></div>
            <div className="tech tech-ccavenue"></div>
            <div className="tech tech-google_plus"></div>
            <div className="tech tech-google-adsense"></div>
            <div className="tech tech-google-analytics"></div>
          </div>
          <h3>
            Software Version Control System
          </h3>
          <div className="logos text-center">
            <div className="tech tech-git"></div>
            <div className="tech tech-github"></div>
            <div className="tech tech-bitbucket"></div>
          </div>
          <h3>
            Deploy
          </h3>
          <div className="logos text-center">
            <div className="tech tech-aws"></div>
            <div className="tech tech-ocean"></div>
            <div className="tech tech-nginx"></div>
            <div className="tech tech-heroku"></div>
          </div>
          <h3>
            Project Management
          </h3>
          <div className="logos text-center">
            <div className="tech tech-dropbox"></div>
            <div className="tech tech-trello"></div>
            <div className="tech tech-pivotal-tracker"></div>
          </div>
        </div>
      </section>
    );
  }
}

export default Techologies;
