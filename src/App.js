import React, { Component } from 'react';
import NavBar from './navbar/NavBar.js';
import Header from './header/Header.js';
import Projects from './projects/Projects.js';
import Service from './service/Service.js';
import Techologies from './technologies/Techologies.js';
import About from './about/About.js';
import Reviews from './reviews/Reviews.js';
import Contact from './contact/Contact.js';
import Footer from './footer/Footer.js';
import Favicon from 'react-favicon';
import favicon from './images/favicon.ico';

import './styles/application.scss';

class App extends Component {
  render() {
    return (
      <div>
        <Favicon url={favicon}/>
        <NavBar/>
        <Header/>
        <Projects/>
        <Service/>
        <Techologies/>
        <About/>
        <Reviews/>
        <Contact/>
        <Footer/>
      </div>
    );
  }
}

export default App;
