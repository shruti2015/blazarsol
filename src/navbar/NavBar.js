import React, { Component } from 'react';
import './cbpAnimatedHeader.js';
/* global $ */

class NavBar extends Component {
  componentDidMount(){
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top'
    })

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();  
    });


  }

  render() {
    return (
      <nav className="navbar navbar-default navbar-fixed-top">
        <div className="container">
          <div className="navbar-header page-scroll">
            <button className="navbar-toggle" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <a className="navbar-brand page-scroll" href="#page-top">
              <span className="icon-logo"></span>
              <span className="text">
                <span className="icon-blazar"></span>
                <br/>
                <span className="icon-ss"></span>
              </span>
            </a>
          </div>
          <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul className="nav navbar-nav navbar-right">
              <li className="hidden active">
                <a href="#page-top"></a>
              </li>
              <li>
                <a className="page-scroll hello" href="#projects">Projects</a>
              </li>
              <li>
                <a className="page-scroll" href="#services">Services</a>
              </li>
              <li>
                <a className="page-scroll" href="#technologies">Technologies</a>
              </li>
              <li>
                <a className="page-scroll" href="#about">About</a>
              </li>
              <li>
                <a className="page-scroll" href="#reviews">Reviews</a>
              </li>
              <li>
                <a className="page-scroll" href="#contact">Contact</a>
              </li>
              <li>
                <a className="page-scroll" href="http://blazarblogs.wordpress.com" target="_blank">Blogs</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default NavBar;
